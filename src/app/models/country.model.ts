// Country model for display only necessary data
export interface Country {
    name?: string;
    capital?: string;
    population?: number;
    subregion?: string;
}
