import { TestBed, inject } from '@angular/core/testing';
import {
    HttpClientTestingModule,
    HttpTestingController
} from '@angular/common/http/testing';
import { CountryService } from './country.service';
import { Country } from '../models/country.model';

describe('CountryService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [CountryService]
        });
    });
    // Test Case success
    it(
        'Should get countries',
        inject(
            [HttpTestingController, CountryService],
            (httpMock: HttpTestingController, countryService: CountryService) => {
                const mockCountries: Country[] = [
                    { name: 'Afghanistan', capital: 'Kabul', population: 27657145, subregion: 'Southern Asia' },
                    { name: 'Albania', capital: 'Tirana', population: 2886026, subregion: 'Southern Europe' },
                    { name: 'Algeria', capital: 'Algiers', population: 40400000, subregion: 'Northern Africa' }
                ];
                countryService.listCountry().subscribe(() => {});

                const mockReq = httpMock.expectOne(`${countryService.apiURL}/all`);

                expect(mockReq.cancelled).toBeFalsy();
                expect(mockReq.request.responseType).toEqual('json');
                mockReq.flush(mockCountries);
                httpMock.verify();
            }
        )
    );

    // Test Case error
    it(
        'Should get error',
        inject(
            [CountryService],
            ( countryService: CountryService) => {
                const mockErrorResponse = { status: 201, message: 'Data Not found' };
                const errorMessageThrow = `Error Code: ${mockErrorResponse.status}\nMessage: ${mockErrorResponse.message}`;
                countryService.handleError(mockErrorResponse).subscribe(
                    null,
                    error => {
                        expect(error).toEqual(errorMessageThrow);
                    }
                );
            }
        )
    );
});
