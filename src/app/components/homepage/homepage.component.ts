import { Component, OnInit } from '@angular/core';
import { CountryService } from 'src/app/services/country.service';
import { BehaviorSubject } from 'rxjs';
import { Country } from 'src/app/models/country.model';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
  countries$ = new BehaviorSubject([]);
  constructor(
    private countryService: CountryService
  ) { }

  ngOnInit() {
    this.countryService.listCountry().subscribe((res: Country[]) => this.countries$.next(res));
  }
}
