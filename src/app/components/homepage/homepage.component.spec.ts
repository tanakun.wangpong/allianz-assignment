import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HomepageComponent } from './homepage.component';
import { MatFormFieldModule, MatProgressSpinnerModule, MatCardModule } from '@angular/material';
import { ListItemComponent } from '../list-item/list-item.component';
import { FilterPipe } from 'src/app/pipes/filter.pipe';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { CountryService } from 'src/app/services/country.service';
import { mocked } from 'ts-jest/utils';
import { Country } from 'src/app/models/country.model';

jest.mock('src/app/services/country.service');
const mockService = mocked(CountryService, true);

describe('HomepageComponent', () => {
    let component: HomepageComponent;
    let fixture: ComponentFixture<HomepageComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, MatFormFieldModule, MatProgressSpinnerModule, MatCardModule],
            declarations: [HomepageComponent, ListItemComponent, FilterPipe],
            providers: [mockService],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        mockService.mockClear();
        mockService.prototype.listCountry.mockImplementation(() => of([]));
        fixture = TestBed.createComponent(HomepageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('Should match homepage component', () => {
        component.countries$ = new BehaviorSubject([]);
        fixture.detectChanges();
        (expect(fixture) as any).toMatchSnapshot();

    });
});
