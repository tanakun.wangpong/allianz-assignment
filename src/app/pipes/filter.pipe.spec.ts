import { FilterPipe } from './filter.pipe';

describe('Pipe: FilterPipe', () => {
    let pipe: FilterPipe;

    beforeEach(() => {
        pipe = new FilterPipe();
    });

    it('should be filter and display full match result', () => {
        expect(pipe.transform([{name: 'Afghanistan'}, {name: 'Albania'}], 'Albania')).toEqual([{name: 'Albania'}]);
    });

    it('should be filter and display a part match result', () => {
        expect(pipe.transform([{name: 'Afghanistan'}, {name: 'Albania'}], 'Al')).toEqual([{name: 'Albania'}]);
    });

    it('should be filter and display unmatch result', () => {
        expect(pipe.transform([{name: 'Afghanistan'}, {name: 'Albania'}], 'Algeria')).toEqual([]);
    });
});
