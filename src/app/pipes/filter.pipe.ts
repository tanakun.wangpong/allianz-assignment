import { Pipe, PipeTransform } from '@angular/core';
import { Country } from '../models/country.model';
@Pipe({
    name: 'filter'
})
// FilterPipe for filter country with name
export class FilterPipe implements PipeTransform {
    transform(countries: Country[], searchText: string): Country[] {
        if (!countries) { return []; }
        if (!searchText) { return countries; }
        searchText = searchText.toLowerCase();
        return countries.filter(country => {
            return country.name.toLowerCase().includes(searchText);
        });
    }
}
